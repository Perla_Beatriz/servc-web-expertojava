package banco;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class Conexion {
    Connection con;
    String url="jdbc:oracle:thin:@localhost:1521:XE";
    String user="Cajero";
    String pass="cajero";
    
    public Connection Conectar() throws SQLException{
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            con=DriverManager.getConnection(url,user,pass);
            con.setAutoCommit(false);
            if(con != null){
            
            System.out.println("Conexion Exitosa");
       
            }else {
            
             System.out.println("Conexion invalida");
            
            }
        } 
        catch (Exception e) {
            
            JOptionPane.showMessageDialog(null,"Conexion invalida"+ e.getMessage());
        }      
        return con;
        
    }
    
    public void desconexion(){
    try{
    con.close();
    
    }
    catch (Exception e){
     System.out.println("Error al conectar"+ e.getMessage());
    
    }
    
    
   }
    
    
    public static void main(String[] args) throws SQLException
    {
     Conexion cone= new Conexion();
     cone.Conectar();
    
    }
    
}
 